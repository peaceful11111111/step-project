const web = document.querySelector('.web-btn');
const graphic = document.querySelector('.graphic-btn');
const support = document.querySelector('.support-btn');
const app = document.querySelector('.app-btn');
const marketing = document.querySelector('.marketing-btn');
const seo = document.querySelector('.seo-btn');
const servicesContentDiv = document.querySelectorAll('.services-content-div');

const webContent = document.querySelector('.web');
const graphicContent = document.querySelector('.graphic');
const supportContent = document.querySelector('.support');
const appContent = document.querySelector('.app');
const marketingContent = document.querySelector('.marketing');
const seoContent = document.querySelector('.seo');

const servicesTitle = document.querySelector('.services-titles');


servicesTitle.addEventListener('click', (event) => {
  toggleText(event);
});

function toggleText(browserEvent) {
  const clickedElement = browserEvent.target;

  let currentActiveTab = document.querySelector('.services-tabs.active-btn');
  currentActiveTab.classList.remove('active-btn');
  clickedElement.classList.add('active-btn');

  let webVisibleContent = document.querySelector('.web');

  if (clickedElement === web) {
    webVisibleContent.style.display = 'flex';
    servicesContentDiv.forEach(elem => {
      elem.classList.remove('active');
      elem.hidden = true;
    });
    webContent.classList.add('active');
    webContent.removeAttribute('hidden')
  } else if (clickedElement === graphic) {
    webVisibleContent.style.display = 'none';
    servicesContentDiv.forEach(elem => {
      elem.classList.remove('active');
      elem.hidden = true;
    });
    graphicContent.classList.add('active');
    graphicContent.removeAttribute('hidden')
  } else if (clickedElement === support) {
    webVisibleContent.style.display = 'none';
    servicesContentDiv.forEach(elem => {
      elem.classList.remove('active');
      elem.hidden = true;
    });
    supportContent.classList.add('active');
    supportContent.removeAttribute('hidden')
  } else if (clickedElement === app) {
    webVisibleContent.style.display = 'none';
    servicesContentDiv.forEach(elem => {
      elem.classList.remove('active');
      elem.hidden = true;
    });
    appContent.classList.add('active');
    appContent.removeAttribute('hidden')
  } else if (clickedElement === marketing) {
    webVisibleContent.style.display = 'none';
    servicesContentDiv.forEach(elem => {
      elem.classList.remove('active');
      elem.hidden = true;
    });
    marketingContent.classList.add('active');
    marketingContent.removeAttribute('hidden')
  } else if (clickedElement === seo) {
    webVisibleContent.style.display = 'none';
    servicesContentDiv.forEach(elem => {
      elem.classList.remove('active');
      elem.hidden = true;
    });
    seoContent.classList.add('active');
    seoContent.removeAttribute('hidden')
  }
}

const allBtn = document.querySelector('.all-btn');
const graphicDesBtn = document.querySelector('.graphic-des-btn');
const webDesBtn = document.querySelector('.web-des-btn');
const landingBtn = document.querySelector('.landing-btn');
const wordpressBtn = document.querySelector('.wordpress-btn');

const allImages = document.querySelectorAll('.all-images');

const workExamplesTitles = document.querySelector('.work-examples-titles');

workExamplesTitles.addEventListener('click', (event) => {
  showMoreImages(event);
});

function showMoreImages(e) {
  const clickedElement = e.target;

  let activeTabs = document.querySelector('.section-menu-link.active-tabs');
  activeTabs.classList.remove('active-tabs');
  clickedElement.classList.add('active-tabs');

  if (clickedElement === allBtn) {
    allImages.forEach(elem => {
      elem.removeAttribute('hidden');
      if (elem.classList.contains('hidden')) {
        elem.setAttribute('hidden', true);
      }
      loadMoreDiv.removeAttribute('hidden');
    });
  } else if (clickedElement === graphicDesBtn) {
    allImages.forEach(elem => {
      elem.setAttribute('hidden', true);
      if (elem.classList.contains('graphic-des-img') && !elem.classList.contains('hidden')) {
        elem.removeAttribute('hidden');
      }
      loadMoreDiv.hidden = true;
    });
  } else if (clickedElement === webDesBtn) {
    allImages.forEach(elem => {
      elem.setAttribute('hidden', true);
      if (elem.classList.contains('web-des-img') && !elem.classList.contains('hidden')) {
        elem.removeAttribute('hidden');
      }
      loadMoreDiv.hidden = true;
    });
  } else if (clickedElement === landingBtn) {
    allImages.forEach(elem => {
      elem.setAttribute('hidden', true);
      if (elem.classList.contains('landing-img') && !elem.classList.contains('hidden')) {
        elem.removeAttribute('hidden');
      }
      loadMoreDiv.hidden = true;
    });
  } else if (clickedElement === wordpressBtn) {
    allImages.forEach(elem => {
      elem.setAttribute('hidden', true);
      if (elem.classList.contains('wordpress-img') && !elem.classList.contains('hidden')) {
        elem.removeAttribute('hidden');
      }
      loadMoreDiv.hidden = true;
    });
  }
}

const loadMoreDiv = document.querySelector('.load-more-div');
const hiddenImages = document.querySelectorAll('.hidden');
const loadMoreBtn = document.querySelector('.load-more-btn');
loadMoreBtn.addEventListener('click', (itIsEvent) => {
  if (itIsEvent.target === loadMoreBtn) {
    hiddenImages.forEach(elem => {
      elem.classList.remove('hidden');
      elem.removeAttribute('hidden');
      loadMoreBtn.remove();
    });
  }
});


const galleryThumbs = new Swiper('.gallery-thumbs', {
  spaceBetween: 36,
  slidesPerView: 4,
  loop: true,
  freeMode: true,
  loopedSlides: 5, //looped slides should be the same
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
  // clickable: true,
});
const galleryTop = new Swiper('.gallery-top', {
  spaceBetween: 36,
  loop: true,
  loopedSlides: 5, //looped slides should be the same
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  thumbs: {
    swiper: galleryThumbs,
  },
});













